package com.servicetick.android.library.entities.triggers

internal class ManualTrigger(presentation: TriggerPresentation, surveyId: Long = -1) : TriggerInt(presentation, "manual", surveyId)