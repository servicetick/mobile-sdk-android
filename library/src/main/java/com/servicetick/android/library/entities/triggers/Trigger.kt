package com.servicetick.android.library.entities.triggers

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.LifecycleOwner
import com.servicetick.android.library.entities.Survey

class Trigger internal constructor(private val trigger: TriggerInt) {

    val tag: String = trigger.tag
    var active: Boolean
        get() = trigger.active
        set(value) {
            trigger.active = value
        }

    @JvmOverloads
    fun launchSurvey(observer: Survey.ExecutionObserver? = null, lifecycleOwner: LifecycleOwner? = null): Fragment? = trigger.launchSurvey(observer, lifecycleOwner)

    fun getData() = trigger.getData()

    fun observe(lifecycleOwner: LifecycleOwner, observer: Trigger.TriggerFiredObserver) = trigger.observe(lifecycleOwner, observer)
    fun observeForever(observer: Trigger.TriggerFiredObserver) = trigger.observeForever(observer)
    fun removeObservers(lifecycleOwner: LifecycleOwner) = trigger.removeObservers(lifecycleOwner)
    fun removeObserver(observer: Trigger.TriggerFiredObserver) = trigger.removeObserver(observer)

    inline fun observe(lifecycleOwner: LifecycleOwner, crossinline action: (trigger: Trigger) -> Unit): Trigger.TriggerFiredObserver {
        val observer = object : Trigger.TriggerFiredObserver {
            override fun triggerFired(trigger: Trigger) {
                action(trigger)
            }
        }
        observe(lifecycleOwner, observer)
        return observer
    }

    interface TriggerFiredObserver : LifecycleObserver {
        fun triggerFired(trigger: Trigger)
    }
}