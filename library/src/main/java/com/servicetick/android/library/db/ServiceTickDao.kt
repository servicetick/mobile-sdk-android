package com.servicetick.android.library.db

import androidx.lifecycle.LiveData
import androidx.room.*
import com.servicetick.android.library.entities.*
import com.servicetick.android.library.entities.db.BaseSurvey
import com.servicetick.android.library.entities.db.BaseSurveyQuestion
import com.servicetick.android.library.entities.db.BaseSurveyResponse
import com.servicetick.android.library.entities.triggers.TriggerInt

@Dao
internal interface ServiceTickDao {

    @Transaction
    fun insert(survey: BaseSurvey) {

        val responses = getSurveyResponses(survey.id)

        survey.pageTransitions.forEachIndexed { index, surveyPageTransition ->
            surveyPageTransition.surveyId = survey.id
            surveyPageTransition.order = index
        }
        survey.questionOptionActions.forEach { questionOptionAction ->
            questionOptionAction.surveyId = survey.id
        }

        insertSurvey(survey)
        insertSurveyPageTransitions(survey.pageTransitions)

        survey.questions.forEach { question ->

            question.surveyId = survey.id
            question.options.forEach { option ->
                option.questionId = question.id

            }
            insertSurveyQuestion(question)
            insertSurveyQuestionOptions(question.options)

            if (question.isAnswerable()) {
                // Check that there is a SurveyResponseAnswer for this Question,
                // it may be a newly added Question
                responses.forEach { response ->
                    // Check is the ResponseAnswer exists, if not build and insert
                    response.answers.firstOrNull {
                        it.surveyQuestionId == question.id
                    } ?: run {
                        SurveyQuestion.buildResponseAnswer(question).apply {
                            surveyResponseId = response.id
                            insertSurveyResponseAnswer(this)
                        }
                    }
                }
            }
        }
        insertSurveyQuestionOptionActions(survey.questionOptionActions)
    }

    @Transaction
    fun insert(surveyResponse: SurveyResponse) {
        val baseSurveyResponse = BaseSurveyResponse(surveyResponse)
        baseSurveyResponse.id = insertSurveyResponse(baseSurveyResponse)

        surveyResponse.answers.forEach {
            it.surveyResponseId = baseSurveyResponse.id
        }
        insertSurveyResponseAnswers(surveyResponse.answers)
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurvey(survey: BaseSurvey)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(trigger: TriggerInt)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(trigger: List<TriggerInt>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyPageTransitions(entities: List<SurveyPageTransition>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyQuestionOptionActions(entities: List<SurveyQuestionOptionAction>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyQuestions(entities: List<BaseSurveyQuestion>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyQuestion(question: BaseSurveyQuestion)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyQuestionOptions(entities: List<SurveyQuestionOption>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyResponse(surveyResponse: BaseSurveyResponse): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyResponseAnswers(surveyResponseAnswers: List<SurveyResponseAnswer>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSurveyResponseAnswer(surveyResponseAnswer: SurveyResponseAnswer)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(entity: BaseSurvey?)

    @Delete
    fun delete(entity: BaseSurvey?)

    @Transaction
    @Query("SELECT * FROM surveys WHERE surveys.id=:id")
    fun getSurvey(id: Long): SurveyInt?

    @Transaction
    @Query("SELECT * FROM surveys WHERE surveys.id=:id")
    fun getSurveyAsLiveData(id: Long): LiveData<SurveyInt?>

    @Transaction
    @Query("SELECT * FROM survey_responses WHERE syncStamp is NULL AND isComplete = 1")
    fun getSyncableSurveyResponse(): List<SurveyResponse?>

    @Transaction
    @Query("SELECT * FROM survey_responses WHERE survey_responses.surveyId=:surveyId")
    fun getSurveyResponses(surveyId: Long): List<SurveyResponse>

    @Transaction
    @Query("SELECT * FROM survey_questions WHERE survey_questions.pageId=:pageId")
    fun getQuestionsForPageAsLiveData(pageId: Long): LiveData<List<SurveyQuestion>>

    @Transaction
    @Query("SELECT * FROM surveys")
    fun getSurveys(): List<SurveyInt>

    @Query("DELETE from survey_questions where surveyId=:surveyId AND id NOT IN(:notInQuestionIds)")
    fun purgeQuestions(surveyId: Long, notInQuestionIds: Array<Long>)

    @Query("UPDATE survey_responses set syncStamp = datetime('now') where id = :surveyResponseId")
    fun markResponseAsSynced(surveyResponseId: Long)

    @Query("UPDATE triggers set active = 0 where surveyId = :surveyId AND tag NOT IN (:tagNotInList)")
    fun disableTriggers(surveyId: Long, tagNotInList: List<String>)

    @Query("UPDATE triggers set data = :data, fired = :fired where tag = :tag")
    fun updateTrigger(tag: String, data: HashMap<String, Any>, fired: Boolean)
}