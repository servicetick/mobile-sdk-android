package com.servicetick.android.library.entities

import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import com.servicetick.android.library.entities.triggers.TriggerPresentation
import com.servicetick.android.library.entities.triggers.Trigger

class Survey internal constructor(private val survey: SurveyInt) {

    fun observeStateChange(lifecycleOwner: LifecycleOwner, observer: Survey.StateChangeObserver) =
            survey.observeStateChange(lifecycleOwner, observer)

    fun observeStateChangeForever(observer: Survey.StateChangeObserver) =
            survey.addStateChangeObserver(observer)

    fun removeStateChangeObservers(lifecycleOwner: LifecycleOwner) =
            survey.lifecycleStateChangeObservers.remove(lifecycleOwner)

    fun removeStateChangeObserver(observer: Survey.StateChangeObserver) =
            survey.foreverStateChangeObservers.remove(observer)

    fun getAllTriggers() = survey.getAllTriggers().map { Trigger(it) }

    fun getTrigger(triggerTag: String) = survey.getTrigger(triggerTag)?.let { Trigger(it) }

    @JvmOverloads
    fun start(presentation: TriggerPresentation = TriggerPresentation.START_ACTIVITY, observer: Survey.ExecutionObserver? = null, lifecycleOwner: LifecycleOwner? = null): Fragment? =
            survey.start(presentation, observer, lifecycleOwner)

    enum class State {

        /**
         * The Survey has been queued for initialisation
         */
        ENQUEUED,

        /**
         * The Survey has been initialised and ready to use
         */
        INITIALISED,

        /**
         * The Survey has been disabled
         */
        DISABLED
    }

    interface ExecutionObserver {
        fun onPageChange(newPage: Int, oldPage: Int)
        fun onSurveyComplete()
        fun onSurveyAlreadyComplete()
    }

    interface StateChangeObserver {
        fun onSurveyStateChange(surveyState: Survey.State, survey: Survey?)
    }

    companion object {
        internal val DEFAULT_REFRESH_INTERVAL = SurveyInt.DEFAULT_REFRESH_INTERVAL
        internal val MINIMUM_REFRESH_INTERVAL = SurveyInt.MINIMUM_REFRESH_INTERVAL
    }
}