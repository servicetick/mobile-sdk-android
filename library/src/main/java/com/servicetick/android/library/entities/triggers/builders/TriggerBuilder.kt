package com.servicetick.android.library.entities.triggers.builders

import com.servicetick.android.library.entities.triggers.ManualTrigger
import com.servicetick.android.library.entities.triggers.TriggerInt
import com.servicetick.android.library.entities.triggers.TriggerPresentation

open class TriggerBuilder {

    @JvmSynthetic
    internal open fun build(): TriggerInt {
        return ManualTrigger(TriggerPresentation.START_ACTIVITY)
    }
}